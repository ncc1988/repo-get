Development on this script has just started so it cannot do much at the moment.
What you can do is fetching all issues (with all comments) of a gitea repository.
They are output on stdout in markdown syntax so that you can generate a
HTML page out of it using pandoc etc.

How to fetch all issues with this script:

    repo-get.rb issues https://example.org/user/repo.git

The ".git" at the end of the gitea repository URL is optional as it is stripped
from the path anyway.
