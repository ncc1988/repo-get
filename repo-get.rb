#!/usr/bin/env ruby
# coding: utf-8


# TODO: License-Text. License: GPLv3


require 'net/http'
require 'json'


if (ARGV.length < 2)
    puts('Usage: repo-get.rb COMMAND REPOSITORY_URL')
    return 0
end

command = ARGV[0]
url_string = ARGV[1]

if (!command)
    puts('No command specified!')
    return 1
end

if (!url_string)
    puts('No repository URL specified!')
    return 1
end


#Split the URL: We need the domain and the last two path items.
url = URI(url_string)

domain = url.host
#Strip leading slash and trailing ".git", if any
repo_path = url.path.gsub(/\.git$/, '').gsub(/^\//, '')

if (command == 'issues')
    issues_api_url = URI('https://' + domain + '/api/v1/repos/' + repo_path + '/issues')
    #puts(issues_api_url.to_s())
    #return 0
    issues = JSON.parse(Net::HTTP.get(issues_api_url))

    issues.each { |issue|
        issue_comment_url = URI(
            'https://' + domain + '/api/v1/repos/' + repo_path +
            '/issues/' + issue['number'].to_s() + '/comments'
        )
        comments = JSON.parse(Net::HTTP.get(issue_comment_url))

        puts('# #' + issue['number'].to_s() + ' ' + issue['title'] + "\n\n")
        puts(issue['body'])
        if (comments.length > 0)
            puts("\n## Comments\n")
            comments.each { |comment|
                puts('- ' + comment['body'] + "\n")
            }
            puts("\n")
        end
        puts("\n--------\n\n")
    }
end
